import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SpecialtyCreateDto } from '../dto/specialty.dto';
import { Specialty, SpecialtyDocument } from '../models/Specialty.model';

@Injectable()
export class SpecialtyService {

    constructor(       
        @InjectModel(Specialty.name) private SpecialtyModel: Model<SpecialtyDocument>){            
        
    }

    async create(createSpecialtyDto: SpecialtyCreateDto): Promise<Specialty> {
        const createdSpecialty = new this.SpecialtyModel(createSpecialtyDto);
        return createdSpecialty.save();
    }
    
    async findById(id : string): Promise<Specialty> {
        return this.SpecialtyModel.findById(id).exec();
    }
    
    async findAll(): Promise<Specialty[]> {
        return this.SpecialtyModel.find().exec();
    }

    async findByName(nameSearch : string, exact : boolean = false): Promise<Specialty> {
        if(!exact)
            return this.SpecialtyModel.findOne({name : { $regex : nameSearch}}).exec();
        return this.SpecialtyModel.findOne({name : nameSearch}).exec();
    }
    

    async validatePassword(email:string, password: string): Promise<Specialty> {
        const Specialty = await this.SpecialtyModel.findOne({
            email,password
        }).exec();
        
   
        return Specialty;
    }


   
}

import { BadRequestException, Body, Controller, Get, Post } from '@nestjs/common';
import { SpecialtyCreateDto } from '../dto/specialty.dto';
import { Specialty } from '../models/Specialty.model';
import { SpecialtyService } from '../services/specialty.service';

@Controller('specialty')
export class SpecialtyController {

    constructor(private specialityService : SpecialtyService){

    }

    @Post()
    async create(@Body()speciality : SpecialtyCreateDto) : Promise<Specialty>{
        if((await this.specialityService.findByName(speciality.name)))
            throw new BadRequestException('Especialidade já cadastrada');
            
        return this.specialityService.create(speciality);
    }

    @Get()
    async getAll() : Promise<Specialty[]>{
        return this.specialityService.findAll();
    }
}

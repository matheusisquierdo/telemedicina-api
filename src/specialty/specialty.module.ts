import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SpecialtyController } from './controllers/specialty.controller';
import { Specialty, SpecialtySchema } from './models/specialty.model';
import { SpecialtyService } from './services/specialty.service';

@Module({
  imports : [MongooseModule.forFeature([{ name: Specialty.name, schema: SpecialtySchema }]),],
  providers: [SpecialtyService],
  exports: [SpecialtyService],
  controllers: [SpecialtyController]
})
export class SpecialtyModule {}

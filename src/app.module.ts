import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { LoggerModule } from './logger/logger.module';
import { SpecialtyModule } from './specialty/specialty.module';
import { TeamModule } from './team/team.module';
import { ScheduleModule } from './schedule/schedule.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forRoot(process.env.MONGO_URL,{useNewUrlParser: true, useUnifiedTopology:true}),
    AuthModule, 
     LoggerModule, SpecialtyModule, TeamModule, ScheduleModule,],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

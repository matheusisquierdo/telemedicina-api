import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UserCreateDto } from '../dtos/user.dto';
import { User, UserDocument } from '../models/user.model';

@Injectable()
export class UserService {

    constructor(private configService: ConfigService,        
        @InjectModel(User.name) private userModel: Model<UserDocument>){            
        
    }

    async create(createUserDto: UserCreateDto): Promise<User> {
        const createdUser = new this.userModel(createUserDto);
        return createdUser.save();
    }
    
    
    async findAll(): Promise<User[]> {
        return this.userModel.find().exec();
    }

    async findByUsername(email : string): Promise<User> {
        return this.userModel.findOne({email}).exec();
    }

    async validatePassword(email:string, password: string): Promise<User> {
        const user = await this.userModel.findOne({
            email,password
        }).exec();
        
   
        return user;
    }


   
}

import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { User } from '../models/user.model';
import { UserService } from './user.service';

@Injectable()
export class AuthService {
    
    constructor(private configService: ConfigService,
        private jwtService: JwtService,
        private userService: UserService){            
        
    }
   

    async authenticate(email:string, password: string): Promise<User> {
        const user = await this.userService.validatePassword(email,password);
        return user;
    }


    async login(user: any) {
        //console.log(user);
        const payload = { email: user.email, sub: user.id };
        return {
          access_token: this.jwtService.sign(payload),
        };
      }
}

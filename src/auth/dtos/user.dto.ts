export interface UserCreateDto {
    name: string,
    email: string,
    type:string,
    password:string
}
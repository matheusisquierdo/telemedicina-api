import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from '../services/auth.service';
import { User } from '../models/user.model';


@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super();
  }

  async validate(email: string, pwd: string): Promise<any> {
    const user : User = await this.authService.authenticate(email, pwd);
    if (!user) {
      throw new UnauthorizedException();
    }
  //  const {password, ...rest} =user;
    delete user.password;
    console.log(user);
    return user;
  }
}
import { Module } from '@nestjs/common';
import { ConfigModule, registerAs } from '@nestjs/config';
import { AuthService } from './services/auth.service';
import { AuthController } from './controllers/auth.controller';
import authConfig from './config/auth.config';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from './models/user.model';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './strategy/local.strategy';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './strategy/jwt.strategy';
import { UserController } from './controllers/user.controller';
import { UserService } from './services/user.service';


@Module({
  imports:[
    ConfigModule.forFeature(authConfig),
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '60s' },
    }),
    PassportModule,
  ],
  providers: [AuthService, LocalStrategy, JwtStrategy,UserService, ],
  exports: [UserService],
  controllers: [AuthController, UserController]
})
export class AuthModule {}

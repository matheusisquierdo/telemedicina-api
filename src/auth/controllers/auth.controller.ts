import { Controller, Get, Post, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from '../guards/jwt.guard';
import { LocalAuthGuard } from '../guards/local.guard';
import { User } from '../models/user.model';
import { AuthService } from '../services/auth.service';

@Controller('auth')
export class AuthController {

    constructor(private authService : AuthService){

    }

    @UseGuards(LocalAuthGuard)
    @Post('token')
    async login(@Request() req) {
        return this.authService.login(req.user);
    }
   
}

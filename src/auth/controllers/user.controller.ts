import { Controller, Get, Request, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../guards/jwt.guard';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';

@Controller('user')
export class UserController {

    constructor(private userService : UserService){

    }
    @Get('profile')
    @UseGuards(JwtAuthGuard)
    async getProfile(@Request() req) : Promise<any>{
        const user = await this.userService.findByUsername(req.user.email); 
        const result = {...req.user, name: user.name, roles : [user.type],}
        return result;
    }
}

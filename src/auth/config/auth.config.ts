import { registerAs } from "@nestjs/config";

export default registerAs('database', () => ({
    jwtSecret: process.env.JWT_SECRET,    
  }));
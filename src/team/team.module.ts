import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from 'src/auth/auth.module';
import { SpecialtyModule } from 'src/specialty/specialty.module';
import { TeamController } from './controllers/team.controller';
import { Team, TeamSchema } from './models/team.model';
import { TeamService } from './services/team.service';

@Module({
  imports : [MongooseModule.forFeature([{ name: Team.name, schema: TeamSchema }]),
  SpecialtyModule,AuthModule, ],
  providers: [TeamService],
  controllers: [TeamController]
})
export class TeamModule {}

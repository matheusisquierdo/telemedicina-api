import { BadRequestException, Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { TeamAddMember, TeamCreateDto } from '../dto/Team.dto';
import { TeamService } from '../services/team.service';

@Controller('team')
export class TeamController {

    constructor(private teamService : TeamService){

    }

    @Post()
    async create(@Body()team : TeamCreateDto) : Promise<any>{

        if((await this.teamService.findByName(team.name)))
            throw new BadRequestException('Já existe um time com este nome');
        return this.teamService.create(team);
    }

    @Post(':teamName/member')
    async addMember(@Body()addMember : TeamAddMember, @Param('teamName')teamName : string) : Promise<any>{

        
        return this.teamService.addMember(teamName, addMember.email);
    }

    @Get()
    async getAll() : Promise<any>{
        return this.teamService.findAll();
    }
}

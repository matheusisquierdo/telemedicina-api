import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { Specialty } from 'src/specialty/models/Specialty.model';
import { User } from 'src/auth/models/user.model';

export type TeamDocument = Team & Document;

@Schema()
export class Team {

  _id: string;

  @Prop({ required: true, unique: true })
  name: string;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Specialty' })
  specialty: Specialty;  
  
  @Prop({type : [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]})
  members: User[];   
}

export const TeamSchema = SchemaFactory.createForClass(Team);
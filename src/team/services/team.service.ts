import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from 'src/auth/models/user.model';
import { UserService } from 'src/auth/services/user.service';
import { SpecialtyService } from 'src/specialty/services/specialty.service';
import { TeamCreateDto } from '../dto/Team.dto';
import { Team, TeamDocument } from '../models/Team.model';

@Injectable()
export class TeamService {

    constructor(       
        private specialityService : SpecialtyService,
        private userService : UserService,
        @InjectModel(Team.name) private TeamModel: Model<TeamDocument>){            
        
    }

    async create(createTeamDto: TeamCreateDto): Promise<Team> {

        const speciality = await this.specialityService.findByName(createTeamDto.specialty, true);
     
        if(!speciality)
        {
            throw new BadRequestException('Especialidade inválida');
        }

        let uniqueMembers = [...new Set(createTeamDto.members)];
        const queryMembers = uniqueMembers.map(async email => {const user = await this.userService.findByUsername(email);
            if(!user || user.email != email)
                throw new NotFoundException(`Não encontrado usuário cadastrado para ${email}`);
            return user;
        }); 
        
        try{
            const resultMembers = await Promise.all(queryMembers);            
            const createdTeam = new this.TeamModel(createTeamDto);
            createdTeam.members = resultMembers;
            console.log(resultMembers);
            createdTeam.specialty = speciality;

            return createdTeam.save();
        }
        catch(e)
        {
            throw new BadRequestException(e, 'Nao foi possivel buscar dados dos membros informados');
        }
        

    }


    async addMember(teamName : string, email: string){
        const team = await this.findByName(teamName);
        const member = await this.userService.findByUsername(email);
        
        if(!team && !member)
            throw new NotFoundException(null, "Time e membro não encontrados");
        else if(!member)
            throw new NotFoundException(null, "Membro não encontrados");
        else if(!team)
            throw new NotFoundException(null, "Time não encontrados");
       
        
        const memberFinded = team.members.find(tm => tm.email == email);        
        if(memberFinded && memberFinded.email == email)
            throw new BadRequestException('Membro já vinculado a este time');

        team.members.push(member);
        await this.TeamModel.updateOne({_id : team._id}, {members : team.members}, {upsert: true});
        return await this.findByName(teamName);
    }

    async findByName(nameSearch : string): Promise<Team> {
        const team = this.TeamModel.findOne({name : { '$regex' : nameSearch}}).populate('speciality').populate('members').exec()
        console.log(team);
        return team;
    }

    async findAll(): Promise<Team[]> {
        return this.TeamModel.find().exec();
    }

    async findByTeamname(email : string): Promise<Team> {
        return this.TeamModel.findOne({email}).exec();
    }

    async validatePassword(email:string, password: string): Promise<Team> {
        const Team = await this.TeamModel.findOne({
            email,password
        }).exec();
        
   
        return Team;
    }


   
}

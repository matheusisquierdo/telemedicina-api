export interface TeamCreateDto {
    name: string, 
    specialty: string,
    members : string[]
}

export interface TeamAddMember{
    email : string
}
import { BadRequestException, Inject, Injectable, OnModuleInit } from '@nestjs/common';
import * as moment from 'moment-timezone';
import { Specialty } from 'src/specialty/models/Specialty.model';
import { SpecialtyService } from 'src/specialty/services/specialty.service';
import { SchedulingAvailable } from '../dtos/avaiability-scheduling.dto';
import { v4 as uuidv4 } from 'uuid';
import { UserService } from 'src/auth/services/user.service';

@Injectable()
export class SchedulingService implements OnModuleInit {

    private avaiabilities : SchedulingAvailable[] = [];

    constructor( @Inject('MomentWrapper') private momentWrapper: moment.Moment,
    private userService : UserService,
    private specialtyService : SpecialtyService){
        
    }

    async onModuleInit(){
        const specialties = await this.specialtyService.findAll();
        specialties.map(sp =>this.initAvaiability(sp));
        
    }

    avaiability(specialty : string){
        return this.avaiabilities.filter(a => a.isBusy == false && (specialty == undefined || specialty.toLowerCase() == a.specialty.name.toLowerCase()));
    }

    initAvaiability(specialty: Specialty){
        const today = moment();
        const from_date = moment().startOf('isoWeek');
        const to_date = moment().endOf('isoWeek').add(-2,'days');
        //const avaiabilities : SchedulingAvailable[] = [];

        for(let iDay = 0; iDay< 5; iDay++){

            let current = from_date;
            const start = current.startOf('day').add(7, 'hours');
    
            const times = 13 * 4; // 24 hours * 15 mins in an hour
            const result= [];
            for (let i = 0; i < times; i++) {
                const scheduleStart = moment(start)
                .add(15 * i, 'minutes');
    
    
                this.avaiabilities.push({
                    start: scheduleStart.format('HH:mm'),
                    end: scheduleStart.add(15,'minutes').format('HH:mm'),
                    date : moment(start).format('YYYY-MM-DD'),
                    isBusy: false,
                    specialty: specialty,
                    id: uuidv4()
                })
                //.format('hh:mm A');
    
          
            }

            from_date.add(1, 'days');
    

        }
        
      
        return this.avaiabilities;
    }

    async schedule(email: string, scheduleId: string) : Promise<SchedulingAvailable>{
        const member = await this.userService.findByUsername(email);

        if(member.type != 'patient')
        {
            throw new BadRequestException(null, 'Usuário não é um paciente');
        }

        var schedule = this.avaiabilities.find(a => a.id == scheduleId);

        if(schedule.isBusy)
        {
            throw new BadRequestException(null, 'Agenda ocupada');
        }

        if(schedule){
            schedule.isBusy = true;
            schedule.patientId = member;
        }

        return schedule;
    
    
    
    
    
    
    }
}

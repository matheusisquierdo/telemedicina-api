import { Module, Scope } from '@nestjs/common';
import * as moment from 'moment';
import { AuthModule } from 'src/auth/auth.module';
import { SpecialtyModule } from 'src/specialty/specialty.module';
import { ScheduleController } from './controllers/schedule.controller';
import { SchedulingService } from './services/scheduling.service';

@Module({
  controllers: [ScheduleController,],
  imports: [
    AuthModule,
    SpecialtyModule,    
  ],
  providers: [
    {
      provide: 'MomentWrapper',
      useValue: moment
    },
    SchedulingService,
  ],
})
export class ScheduleModule {}

import { Controller, Get, Param, Put, Request, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/guards/jwt.guard';
import { SchedulingAvailable } from '../dtos/avaiability-scheduling.dto';
import { SchedulingService } from '../services/scheduling.service';

@Controller('scheduling')
export class ScheduleController {

    constructor(private scheduleService : SchedulingService){
        
    }
    @Get('avaibilities/:specialty')
    async avaibilities(@Param('specialty')specialty : string) : Promise<any>{
        console.log(specialty);
        return await this.scheduleService.avaiability(specialty);
    }

    @UseGuards(JwtAuthGuard)
    @Put(':scheduleId/schedule')
    async schedule(@Request()req,@Param('scheduleId')scheduleId: string ) : Promise<SchedulingAvailable>{
        return this.scheduleService.schedule(req.user.id,scheduleId);
    }
}
